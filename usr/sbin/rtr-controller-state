#!/bin/sh

# /usr/sbin/rtr-controller-state

set -eu

export LC_ALL=C

this="$(readlink -f "$0")"
readonly this="${this}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: ${whatami} [OPTION]...
Export and/or import controller state.

Options:

    -h             print usage and exit
    -i ZIP_FILE    import from zip file
    -o ZIP_FILE    export to zip file
    -t             perform self-test and exit

Notes:

    * each option may be given at most once

    * all options are mutually exclusive

    * associated services are stopped before operations

    * associated services are started after operations

    * associated destinations are deleted before import

Examples:

    # ${whatami} -h

    # ${whatami} -o archive.zip
    
    # ${whatami} -i archive.zip

    # ${whatami} -t

EOF
}

# $1 : outfile
# $2 : path
export_path() {
    zip --verbose --recurse-paths "$1" "$2"
}

# $1 : infile
# $2 : path
import_path() {
    if ! [ -e "$2" ]; then
        error "missing path: $2"
        return 1
    fi
    if ! [ -d "$2" ]; then
        error "not a directory: $2"
        return 1
    fi
    # shellcheck disable=SC2039,SC3043
    local path_uid=""
    if ! path_uid="$(stat -c%u "$2")"; then
        error "FAILURE: stat -c%u $2"
        return 1
    fi
    # shellcheck disable=SC2039,SC3043
    local path_gid=""
    if ! path_gid="$(stat -c%g "$2")"; then
        error "FAILURE: stat -c%g $2"
        return 1
    fi

    # nuke the destination
    find "$2" -mindepth 1 -exec rm -vrf {} + >&2

    # list the members of the given infile, grep files (not directories) that
    # match the given path (sans the leading slash), pass results as args to
    # unzip
    unzip -Z1 "$1" \
        | grep "^${2#/}.*[^/]$" \
        | xargs -r unzip -d / "$1" >&2

    # fix up the ownership and mode bits
    find "$2" \
        -exec chown -c "${path_uid}":"${path_gid}" {} + \
        \( \( -type d -exec chmod -c 0755 {} + \) \
        -o \( -type f -exec chmod -c 0644 {} + \) \) >&2

    return "$?"
}

################################################################################
################################################################################
################################################################################

# vet sudo environment variables
if ! [ 0 -eq "$(id -u)" ]; then
    die "id -u: $(id -u)"
fi
if ! [ 0 -eq "$(id -g)" ]; then
    die "id -g: $(id -g)"
fi
if [ -z "${SUDO_USER-}" ]; then
    die "undefined SUDO_USER"
fi
if [ "root" = "${SUDO_USER}" ]; then
    die "bad SUDO_USER: ${SUDO_USER}"
fi
if [ -z "${SUDO_UID-}" ]; then
    die "undefined SUDO_UID"
fi
if [ 0 -eq "${SUDO_UID}" ]; then
    die "bad SUDO_UID: ${SUDO_UID}"
fi
if [ -z "${SUDO_GID-}" ]; then
    die "undefined SUDO_GID"
fi
if [ 0 -eq "${SUDO_GID}" ]; then
    die "bad SUDO_GID: ${SUDO_GID}"
fi

# discover and vet ${TMPDIR}
TMPDIR="$(dirname "$(mktemp -u)")"
if ! su -c "test -d ${TMPDIR}" "${SUDO_USER}"; then
    die "${SUDO_USER} cannot access directory: ${TMPDIR}"
fi
if ! su -c "test -r ${TMPDIR}" "${SUDO_USER}"; then
    die "${SUDO_USER} cannot read directory: ${TMPDIR}"
fi
if ! su -c "test -w ${TMPDIR}" "${SUDO_USER}"; then
    die "${SUDO_USER} cannot write directory: ${TMPDIR}"
fi
if ! su -c "test -x ${TMPDIR}" "${SUDO_USER}"; then
    die "${SUDO_USER} cannot execute directory: ${TMPDIR}"
fi
# ${TMPDIR} exists and ${SUDO_USER} can use it as such.

# narrow the TMPDIR and install cleanup handler
tmpdir="$(mktemp -dt "${whatami}.XXXXXX")"
readonly tmpdir="${tmpdir}"
chmod 1777 "${tmpdir}"
cleanup() {
    status="$?"
    rm -rf "${tmpdir}"
    return "${status}"
}
trap cleanup EXIT
export TMPDIR="${tmpdir}"

# aggressively limit the arguments because fancy is the enemy
case "$#" in
    0) die "missing arguments" ;;
    1) ;;
    2) ;;
    *) die "too many arguments" ;;
esac

while getopts ":hi:o:t" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        i)
            if ! infile="$(readlink -f "${OPTARG}")"; then
                die "not a path: ${OPTARG}"
            fi
            if ! su -c "test -f ${infile}" "${SUDO_USER}"; then
                die "${SUDO_USER} cannot access file: ${infile}"
            fi
            if ! su -c "test -r ${infile}" "${SUDO_USER}"; then
                die "${SUDO_USER} cannot read file: ${infile}"
            fi
            if ! file -b "${infile}" | grep -Fq 'Zip archive data'; then
                die "$(file "${infile}")"
            fi
            if ! unzip -q -t "${infile}"; then
                die "corrupted zip archive: ${infile}"
            fi
            readonly infile="${infile}"
            ;;
        o)
            # shellcheck disable=SC2039
            if ! outfile="$(readlink -f "${OPTARG}")"; then
                die "bad path: ${OPTARG}"
            fi
            if su -c "test -f ${outfile}" "${SUDO_USER}"; then
                die "file exists: ${outfile}"
            fi
            readonly outfile="${outfile}"
            outdir="$(dirname "${outfile}")"
            readonly outdir="${outdir}"
            if ! su -c "test -d ${outdir}" "${SUDO_USER}"; then
                die "${SUDO_USER} cannot access directory: ${outdir}"
            fi
            if ! su -c "test -r ${outdir}" "${SUDO_USER}"; then
                die "${SUDO_USER} cannot read directory: ${outdir}"
            fi
            if ! su -c "test -w ${outdir}" "${SUDO_USER}"; then
                die "${SUDO_USER} cannot write directory: ${outdir}"
            fi
            if ! su -c "test -x ${outdir}" "${SUDO_USER}"; then
                die "${SUDO_USER} cannot execute directory: ${outdir}"
            fi
            ;;
        t) readonly requested_test="true" ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

if ! [ 0 -eq "$#" ]; then
    die "bad arguments"
fi

if [ "true" = "${requested_test:-false}" ]; then
    info "self-test begin"
    archive="$(mktemp -ut archive.XXXXXX.zip)"
    readonly archive="${archive}"
    "${this}" -h
    "${this}" -o "${archive}"
    "${this}" -i "${archive}"
    info "self-test end"
    exit "$?"
fi

# This assumes that rapidplan and/or rtr-webapps are the only packages web
# should consider.
dpkg_listfiles="$(mktemp -ut dpkg_listfiles.XXXXXX)"
readonly dpkg_listfiles="${dpkg_listfiles}"
dpkg -L rapidplan rtr-webapps >"${dpkg_listfiles}" || true
sort -uo "${dpkg_listfiles}" "${dpkg_listfiles}"

service_unit_list="$(mktemp -t service_unit_list.XXXXXX)"
readonly service_unit_list="${service_unit_list}"
sed -En \
    's,^/lib/systemd/system/([^/]+[.]service)$,\1,gp' \
    <"${dpkg_listfiles}" >"${service_unit_list}"

while read -r service_unit; do
    systemctl stop "${service_unit}" >&2
done <"${service_unit_list}"

path_list="$(mktemp -t path_list.XXXXXX)"
readonly path_list="${path_list}"
cat >"${path_list}" <<'EOF'
/var/lib/rtr_appliance_app/
/var/lib/rtr_spatial_perception/
EOF

if [ -n "${outfile-}" ]; then
    # https://en.wikipedia.org/wiki/Zip_(file_format)#Limits
    # https://stackoverflow.com/questions/29234912/how-to-create-minimum-size-empty-zip-file-which-has-22b
    base64 -d >"${outfile}" <<'EOF'
UEsFBgAAAAAAAAAAAAAAAAAAAAAAAA==
EOF
    while read -r path; do
        if ! export_path "${outfile}" "${path}"; then
            warning "FAILURE: export_path ${outfile} ${path}"
        fi
    done <"${path_list}"
    chown -c "${SUDO_UID}":"${SUDO_GID}" "${outfile}" >&2
    unzip -q -t "${outfile}" >&2 || true
fi

if [ -n "${infile-}" ]; then
    while read -r path; do
        if ! import_path "${infile}" "${path}"; then
            warning "FAILURE: import_path ${infile} ${path}"
        fi
    done <"${path_list}"
fi

while read -r service_unit; do
    systemctl start "${service_unit}" >&2
done <"${service_unit_list}"

exit "$?"
