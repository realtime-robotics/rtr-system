Source: rtr-system
Priority: optional
Maintainer: Neil Roza <neil@rtr.ai>
Build-Depends:
 config-package-dev (>= 5.0),
 debhelper-compat (= 13),
 gettext-base,
 shellcheck <!nocheck>,
 sudo <!nocheck>,
 tzdata (>= 2022),
Standards-Version: 4.6.1
Section: misc
Homepage: https://gitlab.com/realtime-robotics/rtr-system
Vcs-Browser: https://gitlab.com/realtime-robotics/rtr-system
Vcs-Git: https://gitlab.com/realtime-robotics/rtr-system.git

Package: rtr-system-all
Architecture: all
Conflicts:
 rapidplan (<< 2.2.0),
Depends:
 rtr-system-config (= ${binary:Version}),
 rtr-system-tools (= ${binary:Version}),
 rtr-system-udev (= ${binary:Version}),
 ${misc:Depends},
Description: rapidplan-friendly system minutia
 rtr-system is a collection of config files, command-line utilities, and
 various sundry fragments to support and facilitate an extant rapidplan
 installation.
 .
 This package is a metapackage for all the other packages provided by the
 ${S:Source} source.

Package: rtr-system-config
Architecture: all
Conflicts:
 rapidplan (<< 2.2.0),
Depends:
 ${misc:Depends},
Description: rapidplan-friendly system minutia
 rtr-system is a collection of config files, command-line utilities, and
 various sundry fragments to support and facilitate an extant rapidplan
 installation.
 .
 This package contains the configuration files.

Package: rtr-system-tools
Architecture: all
Conflicts:
 rapidplan (<< 2.2.0),
Depends:
 rtr-system-config (= ${binary:Version}),
 unzip,
 zip,
 ${misc:Depends},
Description: rapidplan-friendly system minutia
 rtr-system is a collection of config files, command-line utilities, and
 various sundry fragments to support and facilitate an extant rapidplan
 installation.
 .
 This package contains the command-line utilities.

Package: rtr-system-udev
Architecture: all
Conflicts:
 rapidplan (<< 2.2.0),
Depends:
 dmidecode,
 ${misc:Depends},
Description: rapidplan-friendly system minutia
 rtr-system is a collection of config files, command-line utilities, and
 various sundry fragments to support and facilitate an extant rapidplan
 installation.
 .
 This package contains the udev rules and support scripts.
