# rtr-system/Makefile

confs = \
  etc/chromium-browser/default.rtr-system \
  etc/chromium-browser/policies/managed/chromium-browser.json.rtr-system \
  etc/dconf/db/local.d/00-screensaver \
  etc/dconf/profile/user.rtr-system \
  etc/lightdm/lightdm.conf.d/00-xserver-command.conf \
  etc/skel/.sudo_as_admin_successful \
  etc/sudoers.d/99-coredumpctl \
  etc/sudoers.d/99-cp-license \
  etc/sudoers.d/99-dmidecode \
  etc/sudoers.d/99-lshw \
  etc/sudoers.d/99-rtr-controller-state \
  etc/sudoers.d/99-rtr-debug \
  etc/sudoers.d/99-timedatectl-set-timezone \
  etc/systemd/coredump.conf.d/99-rtr-size-max.conf.rtr-system \
  etc/update-manager/release-upgrades.rtr-system \
  etc/update-motd.d/99-realtime-robotics

progs = \
  lib/udev/is-rtr-controller \
  usr/bin/rtr-log-export \
  usr/bin/rtr-vet-controller \
  usr/sbin/rtr-controller-state \
  usr/sbin/rtr-debug \
  usr/sbin/rtr-init

# We need these for the expansions of the 'etc/sudoers.d/*.envsubst' files.
SHA512SUM = $(shell sha512sum $(1) | grep -Eo '^[[:xdigit:]]{128}')
export RTR_DEBUG_SHA512 = $(call SHA512SUM, usr/sbin/rtr-debug)
export RTR_CONTROLLER_STATE_SHA512 = $(call SHA512SUM, usr/sbin/rtr-controller-state)

INSTALL ?= install

.PHONY: all check install clean

all: $(confs) $(progs)

# https://manpages.ubuntu.com/manpages/focal/man5/sudoers.5.html
#
# The #includedir directive can be used to create a sudoers.d directory that
# the system package manager can drop sudoers file rules into as part of
# package installation. For example, given:
#
#     #includedir /etc/sudoers.d
#
# sudo will suspend processing of the current file and read each file in
# /etc/sudoers.d, skipping file names that end in '~' or contain a '.'
# character to avoid causing problems with package manager or editor
# temporary/backup files.
check: all
	for sudoers in $(filter etc/sudoers.d/%, $(confs)); do \
		visudo --strict --check "$${sudoers}" \
	; done
	shellcheck --version
	shellcheck --external-sources --exclude=SC2317 $(progs)

install: $(confs) $(progs)
	for path in $^; do $(INSTALL) -vD $$path $(DESTDIR)/$$path; done
	for path in $^; do chmod -v --reference=$$path $(DESTDIR)/$$path; done

clean:
	find . -type f -name '*.envsubst' | while read -r path; do rm -vf "$${path%.envsubst}"; done
	find . -type f -name '*.gen.sh' | while read -r path; do rm -vf "$${path%.gen.sh}"; done

%: %.envsubst
	envsubst <$< >$@
	chmod -v --reference=$< $@

%: %.gen.sh
	./$< >$@
	chmod -v --reference=$< $@
